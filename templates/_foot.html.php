
      </div>
    </div>
  </div>
  <footer class="footer">
    <div class="row">
      <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2">
        <div class="pull-right version"><a href="https://github.com/rudolfcms/rudolf/releases/tag/<?=VER_NAME;?>"><?=VER_NAME;?></a></div>
        <strong>&copy; <?=date('Y');?> <?=NAME;?></strong>
      </div>
    </div>
  </footer>
  <?php $this->foot->make();?>
  </body>
</html>
